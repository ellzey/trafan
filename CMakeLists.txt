cmake_minimum_required(VERSION 3.5)
project(trafan)

include (CheckIncludeFiles)



find_package(PkgConfig)

pkg_check_modules(GLIB2 REQUIRED glib-2.0)
pkg_check_modules(LIBEVENT REQUIRED libevent)

check_include_files(stdio.h HAVE_stdio)
check_include_files(ctype.h HAVE_ctype)
check_include_files(unistd.h HAVE_unistd)
check_include_files(stdlib.h HAVE_stdlib)
check_include_files(stdint.h HAVE_stdint)
check_include_files(string.h HAVE_string)
check_include_files(inttypes.h HAVE_inttypes)
check_include_files(time.h HAVE_time)
check_include_files(signal.h HAVE_signal)
check_include_files(sys/socket.h HAVE_sys_socket)
check_include_files(netinet/in.h HAVE_netinet_in)
check_include_files(arpa/inet.h HAVE_arpa_inet)
check_include_files(netinet/if_ether.h HAVE_netinet_if_ether)
check_include_files(netinet/udp.h HAVE_netinet_udp)
check_include_files(netinet/tcp.h HAVE_netinet_tcp)
check_include_files(netinet/ip.h HAVE_netinet_ip)
check_include_files(net/ethernet.h HAVE_net_ethernet)
check_include_files(pcap.h HAVE_pcap)

add_definitions(${GLIB2_CFLAGS} ${LIBEVENT_CFLAGS})
add_executable (trafan trafan.c)

target_link_libraries(
	trafan
	pcap
	${GLIB2_LDFLAGS}
	${LIBEVENT_LDFLAGS}
)
